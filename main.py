# This is a project that loads data from boulder bundesliga.

import csv
import dataclasses
from argparse import ArgumentParser

from BblClasses.Saison import Saison
from BblClasses.Station import Station
from BblClasses.Tabelle import Tabelle
from BblData.TabellenReihe import TabellenReihe
from BblClasses.Sportler import BblSportler

BASEURL = "https://boulder-bundesliga.de/"
SPORTLERURL = "sportler/"
STATIONURL = "spieltag/"


def write_boulders_to_file(all_boulders, filename):
    fieldnames = ['station', 'boulder_nr', 'name', 'multi', 'punkte']
    with open(filename, 'w', newline='') as csvfile:
        csv_writer = csv.DictWriter(csvfile, fieldnames=fieldnames, extrasaction='ignore', quotechar='"', quoting=csv.QUOTE_ALL)
        csv_writer.writeheader()
        for name, boulder in all_boulders.items():
            csv_writer.writerow(dataclasses.asdict(boulder))

def write_saeson_to_file(season: Saison, filename: str):
    fieldnames = ['name', 'total', 'total_estimate', 'tops', 'station_1', 'station_2', 'station_3', 'station_4', 'station_5', 'station_6', 'station_7', 'station_8', 'station_9', 'station_10']
    with open(filename, 'w', newline='') as csvfile:
        csv_writer = csv.DictWriter(csvfile, fieldnames=fieldnames, extrasaction='ignore', quotechar='"', quoting=csv.QUOTE_ALL, delimiter=';')
        csv_writer.writeheader()
        punkte_data = season.get_saison_punkte_data()
        for data in punkte_data.values():
            dictt = dataclasses.asdict(data)
            for ii in range(len(dictt['_stations'])):
                dictt[f'station_{ii}'] = dictt['_stations'][ii]
            csv_writer.writerow(dictt)


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("-o", "--objekt", dest="objekt", default="liga")
    parser.add_argument("-n", "--name", dest="name", default="jan-hoyer")
    parser.add_argument("-l", "--liga", dest="liga", default="1H")
    parser.add_argument("-s", "--statistiken", dest="stats", default="False")
    parser.add_argument("-j", "--jahr", dest="jahr", default="22-23")
    args = parser.parse_args()

    if args.objekt == "sportler":
        bbl_sportler = BblSportler(BASEURL + SPORTLERURL, args.name)
        all_b = bbl_sportler.get_all_boulders_form_sportler()
        write_boulders_to_file(all_b, args.name + ".csv")
    elif args.objekt == "liga":
        argliga = args.liga
        argjahr = args.jahr
        saison = Saison()

        for ii in range(10): # change to count stations per saison
            active_liga = Station(BASEURL + STATIONURL, str(ii + 1), argliga, argjahr)
            tabelle = active_liga.get_table_for_league()
            station_data = active_liga.get_station_data()
            saison.add_station(station_data)

        #saison.create_season_leistungen()
        #saison.print_saison_leistungen()

        #saison.create_season_table()
        #saison.print_saison_tabelle()

        saison.create_sportler_punkte()
        saison.print_saison_punkte_data()

        write_saeson_to_file(saison, f'Saison_Liga_{argliga}.csv')

    elif args.objekt == "tabelle": # Under construction, but i forgot what was the plan ;-)
        liga = Tabelle()

        zeile1 = TabellenReihe("Donald Duck", "fake_link", 12.23, 1,2,3)
        zeile2 = TabellenReihe("Daisy Duck", "fake_link", 42.23, 3,2,1)
        zeile3 = TabellenReihe("Trick Duck", "fake_link", 22.33, 2,2,2)

        liga.add_reihe(zeile1)
        liga.add_reihe(zeile2)
        liga.add_reihe(zeile3)

        liga.sort('punkte')
        liga.print_table()

    print("Done!")
