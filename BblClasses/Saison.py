from pprint import pprint

from BblData.SportlerPunkteData import SportlerPunkteData
from BblData.StationData import StationData


class Saison:
    def __init__(self):
        self.stations = []
        self.saison_leistungen = {}
        self.saison_tabelle = {}
        self.saison_punkte_data = {}

    def add_station(self, new_station: StationData):
        self.stations.append(new_station)

    def create_season_leistungen(self):

        for station in self.stations:
            for sportler in station.tabelle.get_rows():
                if not sportler.name in self.saison_leistungen:
                    self.saison_leistungen[sportler.name] = [None, None, None, None, None, None, None, None, None, None,
                                                             None]
                self.saison_leistungen[sportler.name][station.nr] = sportler

    def create_season_table(self):
        for station in self.stations:
            for sportler in station.tabelle.get_rows():
                if not sportler.name in self.saison_tabelle:
                    self.saison_tabelle[sportler.name] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
                self.saison_tabelle[sportler.name][station.nr] = sportler.punkte

        for sportler in self.saison_tabelle:
            print(self.saison_tabelle[sportler])
            print(sum(self.saison_tabelle[sportler]))
            self.saison_tabelle[sportler][0] = sum(self.saison_tabelle[sportler])

    def create_sportler_punkte(self):
        for station in self.stations:
            for sportler in station.tabelle.get_rows():
                if not sportler.name in self.saison_punkte_data:
                    self.saison_punkte_data[sportler.name] = SportlerPunkteData(sportler.name, sportler.link)

                # Don't know why this is necessary...
                test = self.saison_punkte_data[sportler.name].stations
                test[station.nr] = sportler.punkte
                self.saison_punkte_data[sportler.name].stations = test

    def print_saison_leistungen(self):
        pprint(self.saison_leistungen)

    def print_saison_tabelle(self):
        pprint(self.saison_tabelle)

    def print_saison_punkte_data(self):
        pprint(self.saison_punkte_data)

    def get_saison_leistungen(self):
        return self.saison_leistungen

    def get_saison_tabelle(self):
        return self.saison_tabelle

    def get_saison_punkte_data(self):
        return self.saison_punkte_data
