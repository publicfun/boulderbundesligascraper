from dataclasses import asdict
from pprint import pprint

from BblData.TabellenReihe import TabellenReihe


class Tabelle:

    def __init__(self):
        self.reihen = []

    def add_reihe(self, new_reihe: TabellenReihe):
        self.reihen.append(new_reihe)

    def sort(self, sorted_by: str = 'punkte'):
        self.reihen.sort(reverse=True, key=lambda x: getattr(x, sorted_by))

    def print_table(self):
        pprint(self.reihen)

    def get_rows(self):
        return self.reihen
