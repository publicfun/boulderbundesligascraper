import urllib.request
from dataclasses import astuple, asdict
from pprint import pprint

import requests
#import requests_cache
from bs4 import BeautifulSoup

from BblClasses.Tabelle import Tabelle
from BblData.StationData import StationData
from BblData.TabellenReihe import TabellenReihe


class Station:

    def __init__(self, url="", station="1", liga="3H", jahr="2122"):
        if jahr == "2122":
            self.url = url + "station{}-{}"
        else:
            self.url = url + "station-{}-{}"

        self.station = station
        self.liga = f'l{liga}'
        self.jahr = jahr
        self.url = self.url.format(station, jahr)
        self.tabelle = Tabelle()

    def get_station_nr(self):
        return self.station

    def get_table_for_league(self):
        all_leagues = {}
        #session = requests_cache.CachedSession('boulderbuli', expire_after=3600)
        session = requests.Session()


        r = session.get(self.url)
        if 200 == r.status_code:
            html = r.text

        # r = urllib.request.urlopen(self.url)
        # if 200 == r.code:
        #     html = r.read().decode('utf-8')

            soup = BeautifulSoup(html, 'html.parser')
            league = soup.find("div", class_=self.liga)

            rows = league.find_all('div', class_='row')

            for row in rows:
                name = row.find('a', class_='searchable').text
                link_name = row.find('a', class_='searchable')['href']
                punkte = row.find('div', class_='total').text
                t, f, z = row.find('div', class_='ftbn').text.split('|')

                self.tabelle.add_reihe(TabellenReihe(name, link_name, float(punkte), int(f), int(t), int(z)))

        else:
            print('Fehler auf Seite: {}'.format(r.url))
            print('StatusCode is {}. Should be 200. No data fetched'.format(r.status_code))

        return self.tabelle

    def get_station_data(self):
        return StationData(int(self.station), self.station, 100, self.tabelle)
