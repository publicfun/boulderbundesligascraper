import re

import requests
#import requests_cache
from bs4 import BeautifulSoup

from BblData.BoulderData import BoulderData


class BblSportler:

    def __init__(self, url="", name=""):
        self.url = url + "{}"
        self.name = name
        self.url = self.url.format(name)

    def get_all_boulders_form_sportler(self):
        all_boulders = {}

        soup = self.get_sportler_soup()

        stations = soup.find_all("div", class_="bbl_slider_slide")
        for station in stations:
            stationnr = station.find("div", class_="bbl_ueberschrift")
            nr = 0
            if stationnr:
                nr = stationnr.a.text.split(" ")[-1]
                if nr == "Finale":
                    nr = 0
                try:
                    nr = int(nr)
                except:
                    nr = 0
            all_boulders.update(self._get_all_boulders_form_station_from_sportler(station, nr))

        return all_boulders

    def _get_all_boulders_form_station_from_sportler(self, in_soup, station=0):
        all_boulders = {}

        boulders = in_soup.find_all("div", class_="single_boulder")
        for boulder in boulders:
            b_nr, b_name = self._get_boulder_name_and_nr(boulder.a.div.text)
            b_multi, b_punkte = self._get_boulder_floats(boulder)
            all_boulders[b_name] = BoulderData(b_name,
                                               boulder_nr=b_nr,
                                               link=boulder.a["href"],
                                               multi=b_multi,
                                               punkte=b_punkte,
                                               station=station
                                               )
            # print(boulder)

        return all_boulders

    def get_statistics_from_sportler(self):
        flash = 0.0
        top = 0.0
        zone = 0.0
        soup = self.get_sportler_soup()
        if soup:
            try:
                flash = float(soup.find("div", class_="val f").text[:-1])
                top = float(soup.find("div", class_="val t").text[:-1])
                zone = float(soup.find("div", class_="val b").text[:-1])
            except AttributeError:
                print(f"Attribute error: {self.name}")
        else:
            print(f"Nicht gefunden: {self.name}")

        return flash, top, zone

    def get_sportler_soup(self):
        print(f"URL: {self.url}")
        #session = requests_cache.CachedSession('boulderbuli', expire_after=3600)
        session = requests.Session()
        r = session.get(self.url)

        if 200 == r.status_code:
            return BeautifulSoup(r.text, 'html.parser')

        else:
            print('Fehler auf Seite: {}'.format(r.url))
            print('StatusCode is {}. Should be 200. No data fetched'.format(r.status_code))
            #raise Exception

    @staticmethod
    def _get_boulder_name_and_nr(input_str=""):
        regex = "^#(\\d+) (.*) -.*"
        ret = re.match(regex, input_str)
        return int(ret[1]), ret[2]

    @staticmethod
    def _get_boulder_floats(boulder):
        print(type(boulder))
        try:
            multi = float(boulder.find("div", class_="val").text)
        except ValueError:
            multi = 0.0
        try:
            punkte = float(boulder.find("div", class_="punkte").text)
        except ValueError:
            punkte = 0.0

        return multi, punkte
