Scraper für die Boulder Bundesliga

Es kann ein CSV für jede Liga erzeugt werden:

    python3 main.py -o liga -l 2H

Es kann ein CSV pro sportler erzeugt werden:

    python3 main.py -o sportler -n max-mustermann


Under Construction:
Es kann eine Tabelle auf stdout ausgegeben werden:

    python3 main.py -o tabelle

Zum Nutzen des caches müssen an 2 Stellen die entsprechenden Zeilen umkommentiert werden:

    import requests
    #import requests_cache

    #session = requests_cache.CachedSession('boulderbuli', expire_after=3600)
    session = requests.Session()