from dataclasses import dataclass


@dataclass
class BoulderData:
    name: str
    link: str
    multi: float = 1.0
    punkte: float = 0.0
    ligen: None = None
    flash: int = 0
    top: int = 0
    zone: int = 0
    null: int = 0
    station: int = 0
    boulder_nr: int = 0





