from dataclasses import dataclass

from BblClasses.Tabelle import Tabelle
from BblData.TabellenReihe import TabellenReihe


@dataclass
class StationData:
    nr: int
    ort: str
    teilnehmer: int
    tabelle: Tabelle