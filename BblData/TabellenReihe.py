from dataclasses import dataclass, field

BOULDERS_PER_STATION = 15
@dataclass
class TabellenReihe:
    name: str = field(compare=False)
    link: str
    punkte: float
    flash: int
    top: int
    zone: int
    null: int = field(init=False)

    def __post_init__(self):
        self.null = BOULDERS_PER_STATION - self.top + self.zone

