from dataclasses import dataclass, field

from BblClasses.Sportler import BblSportler


@dataclass(order=True)
class SportlerPunkteData:
    name: str
    link: str
    total: float = 0
    total_estimate: float = 0
    flashes: float = 0.0
    tops: float = 0.0
    zones: float = 0.0
    _stations: list[float] = field(default_factory=lambda: [0.0] * 11)
    #stations: list[float] = field(init=False, repr=False, default_factory=list)

    sort_index: float = field(init=False, repr=False)

    @property
    def stations(self):
        return self._stations

    @stations.setter
    def stations(self, stations: list[float]):
        # change to how many stations are closed already to calculated how many points (in avarage) are still possible
        STATIONS_CLOSED_ALREADY = 9
        self._stations = stations
        self.calc_total()
        self.calc_estimate(STATIONS_CLOSED_ALREADY)
        if self.zones == 0.0:
            self.get_stats_for_sportler()

    def calc_total(self):
        self.total = round(sum(sorted(self._stations)[-8:]), 2)
        #self.total = sum(self._stations)
        self.sort_index = self.total

    def calc_estimate(self, stations_closed: int = 0):
        total_estimate_stations = self._stations.copy()
        visited_stations = [a for a in self._stations.copy() if a > 0.0]
        for ii in range(stations_closed+1, len(self._stations)):
            if total_estimate_stations[ii] == 0.0:
                try:
                    total_estimate_stations[ii] = sum(visited_stations)/len(visited_stations)
                except ZeroDivisionError:
                    print('ZeroDevisionError:', self.name)
        self.total_estimate = round(sum(sorted(total_estimate_stations)[-8:]), 2)

    def get_stats_for_sportler(self):
        pass
        #sportler = BblSportler(self.link)
        #self.flashes, self.tops, self.zones = sportler.get_statistics_from_sportler()
